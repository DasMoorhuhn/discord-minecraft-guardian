import discord
from discord import Status
from discord import app_commands
from discord import Interaction
from discord import Embed
from discord import Guild
from discord import Color
from discord.activity import Game

import asyncio
import logging
import datetime

from model import *
from settings import *
from secret import TOKEN
from helper import getDateTime

from dbHandler import MongoDB

from minecraft import Server
from minecraft import State

# Init Logger
logger = logging.getLogger('discord')
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(filename='data/log/discord.log', encoding='utf-8', mode='a')
handler.setFormatter(logging.Formatter('%(asctime)s|%(levelname)s|%(name)s|:%(message)s'))
logger.addHandler(handler)

mongo = MongoDB()


class client(discord.Client):
  async def startup(self):
    await self.wait_until_ready()


bot = client(intents=discord.Intents.all())
tree = app_commands.CommandTree(client=bot)
startTime = getDateTime()[2]


async def getServersFromMongo(guild: Guild):
  return mongo.get_servers_for_guild(guildID=guild.id)


async def getMessageFromMessageID(messageID: int, channelID: int, guild: Guild):
  channel = await guild.fetch_channel(channelID)
  return await channel.fetch_message(messageID)


async def createEmbed(data: State, server: dict):
  embed = Embed(timestamp=datetime.datetime.utcnow())
  value = f"`Online`: {data.currentPlayers}/{data.maxPlayers}\n"
  if data.playerList != None: value += f"`Players`: {data.playerList}\n"
  value += f"`Version`: {data.version}\n"
  value += f"`Address`: {data.address}:{data.port}"

  embed.add_field(name=f"Server: {server['name']}", value=value, inline=False)
  return embed


async def fetch(guild: Guild, server: dict, update=False):
  serverObj = Server(url=server['address'], port=server['port'], logger=logger)
  state = await serverObj.fetchData()
  logger.info(f"{state.currentPlayers}/{state.maxPlayers} on {server['name']}")
  if update:
    message = await getMessageFromMessageID(messageID=server['dc_embed_id'], channelID=server['dc_channel_id'],
                                            guild=guild)
    await message.edit(embed=await createEmbed(data=state, server=server))
  else:
    message = await sendMessageInChannel(embed=await createEmbed(data=state, server=server),
                                         channelID=server['dc_channel_id'], guild=guild)
    server['dc_embed_id'] = message.id
    return server


async def observeServers():
  while True:
    async for guild in bot.fetch_guilds():
      serverList = await getServersFromMongo(guild=guild)
      for server in serverList:
        await fetch(guild=guild, server=server, update=True)
    await asyncio.sleep(FETCH_CYCLE_IN_SECONDS)


async def sendMessageInChannel(embed: Embed, channelID: int, guild: Guild):
  channel = await guild.fetch_channel(channelID)
  return await channel.send(embed=embed)


@bot.event
async def on_ready():
  logger.info(f"Logged in as: {bot.user.name} with ID {bot.user.id}")
  await bot.change_presence(activity=Game(name="with zombies"), status=Status.online)
  bot.loop.create_task(await observeServers())
  await tree.sync()


@tree.command(name='add-server', description='...')
@app_commands.describe(address="Host or IP")
@app_commands.describe(port="Port of the server")
@app_commands.describe(name="Name")
@app_commands.describe(channel="Channel ID")
async def slash(interaction: Interaction, address: str, port: int, name: str, channel: str):
  logger.info("Command: add-server")
  guild = interaction.guild
  await interaction.response.send_message(content="Ok")

  server = MC_SERVER
  server['name'] = name
  server['address'] = address
  server['port'] = int(port)
  server['dc_channel_id'] = channel

  server = await fetch(guild=guild, server=server)
  mongo.add_mc_server(col=str(guild.id), data=server)


@tree.command(name='delete-server', description='...')
async def slash(interaction: Interaction):
  logger.info("Command: delete-server")
  await interaction.response.send_message(content="Ok")


@tree.command(name='list-server', description='...')
async def slash(interaction: Interaction):
  await interaction.response.defer(thinking=True)
  logger.info("Command: list-server")
  guild = interaction.guild

  serverList = await getServersFromMongo(guild=guild)
  embed = Embed()
  for server in serverList:
    server: dict
    message = ""
    message += f"Address: {server['address']}:{server['port']}\n"
    message += f"Channel ID: {server['dc_channel_id']}\n"
    embed.add_field(name=f"Server: {server['name']}", value=message, inline=False)

  await interaction.response.send_message(embed=embed)


bot.run(token=TOKEN)
