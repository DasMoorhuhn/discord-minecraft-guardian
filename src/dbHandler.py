from pymongo import *
from os import environ as env


class MongoDB:
  def __init__(self) -> None:
    url = env['MONGO_HOST']
    user = env['MONGO_ROOT']
    password = env['MONGO_PASSWD']
    db = env['MONGO_DB']

    self.__client = MongoClient(host=f"mongodb://{user}:{password}@{url}", port=27017)
    self.__db = self.__client[db]

  def get_collection(self, name: str):
    return self.__db[name]

  def get_servers_for_guild(self, guildID: int):
    collection = self.__db[str(guildID)]
    cursor = collection.find({})
    serverList = []
    for server in cursor:
      serverList.append(server)
    return serverList

  def add_mc_server(self, col: str, data: dict):
    collection = self.__db[col]
    check = self.check_for_entry(col=col, data={"name": data["name"]})

    if isinstance(check, int):
      return collection.insert_one(data).inserted_id
    else:
      return 1

  def delete_mc_server(self, col: str, data):
    collection = self.__db[col]
    check = self.check_for_entry(col=col, data={"name": data["name"]})
    if isinstance(check, int):
      return 1
    else:
      return collection.delete_one({"_id": check["_id"]})

  def check_for_entry(self, col: str, data: dict):
    """Check if an entry is stored

    Args:
        col (str): Name of the collection
        data (dict): Dict to search for

    Returns:
        int:  When no entry was found
        dict: When a entry was found
    """

    collection = self.__db[col]
    if collection.count_documents(data).real != 0:
      return collection.find_one(data)
    else:
      return 0
