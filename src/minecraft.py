import minestat
from mcstatus import JavaServer
from mcstatus import BedrockServer

import logging
from time import sleep


class Server:
  def __init__(self, url: str, port: int, logger: logging) -> None:
    self.__server = minestat.MineStat(address=url, port=port)
    self.__logger = logger
    self.address = url
    self.port = port

  async def fetchData(self):
    self.__server = minestat.MineStat(address=self.address, port=self.port)
    data = {
      "current_players": self.__server.current_players,
      "max_players": self.__server.max_players,
      "player_list": self.__server.player_list,
      "version": self.__server.version,
      "protocol": self.__server.slp_protocol,
      "online": self.__server.player_list,
      "address": self.__server.address,
      "port": self.__server.port,
      "icon": self.__server.favicon
    }
    return State(data=data)


class State:
  def __init__(self, data) -> None:
    self.currentPlayers = data['current_players']
    self.maxPlayers = data['max_players']
    self.playerList = data['player_list']
    self.version = data['version']
    self.protocol = data['protocol']
    self.online = data['online']
    self.address = data['address']
    self.port = data['port']
    self.icon = data['icon']


class Server_Java:
  def __init__(self, url: str, port: int) -> None:
    self.server = JavaServer.lookup(f"{url}:{port}")


class Server_Bedrock:
  def __init__(self, url: str, port: int) -> None:
    self.server = BedrockServer.lookup(f"{url}:{port}")
