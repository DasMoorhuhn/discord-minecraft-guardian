# Discord Minecraft Guardian

This bot sent a notification in a channel if the player counter on a minecraft server changed.

## Getting started

Create a file called `secret.py` in the same directory as the docker-compose.yml file. There create a variable and store ur discord bot token.

```python
TOKEN="YOUR_TOKEN"
```

Then run

```bash
docker-compose up --build
```